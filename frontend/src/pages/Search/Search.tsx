import React from 'react';
import './Search.css';

type SearchPageProps = {};

const SearchPage: React.FC<SearchPageProps> = () => {
  // Create a function to fetch all locations from database

  // Create a function to sort locations by categories & by number of rooms

  // Create a search function linked to the search input in the header

  return <div className="search">{/* List of sorted locations card */}</div>;
};

export default SearchPage;
