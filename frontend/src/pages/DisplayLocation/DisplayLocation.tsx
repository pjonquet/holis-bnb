import React from 'react';

type DisplayLocationPageProps = {};

const DisplayLocationPage: React.FC<DisplayLocationPageProps> = () => {
  // Create a function to handle price change and persist it to database

  // Create a function to delete the location and persist it to database

  return (
    <div className="display-location">
      {/* image */}

      <div className="display-location__content">
        {/* title */}
        {/* property type */}
        {/* category description */}
        {/* price */}
      </div>

      <div className="display-location__edit">
        {/* price input */}
        {/* price button */}
        {/* delete button */}
      </div>
    </div>
  );
};

export default DisplayLocationPage;
