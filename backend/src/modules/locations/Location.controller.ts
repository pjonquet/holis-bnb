import { Controller, Get } from '@nestjs/common';
import { LocationService } from './Location.service';

@Controller('locations')
export class LocationController {
  constructor(private readonly locationService: LocationService) {}

  // This is an example of controller.
  // You don't have to use it or you can modify it.
  /** List all locations in database with this endpoint */
  @Get()
  async getLocations() {
    return await this.locationService.getLocations();
  }
}
